This project describes the [Protractor](https://www.protractortest.org/#/) setup for JavaScript projects including Angular 8 & it's usage.

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

Go to https://musing-khorana-85376c.netlify.com/ to check the demo web app.

## Available Scripts

Launch the terminal, here you can run:

```bash
sudo webdriver-manager start
```

It simply runs the Selenium server in your local machine. *Make sure you have the latest webdriver installed in your system. The webdriver makes the Google Chrome browser compatible with the Protractor framework.*

Go to, [http://localhost:4444/wd/hub/static/resource/hub.html](http://localhost:4444/wd/hub/static/resource/hub.html) and check whether the Selenium server is running or not.

In the project directory, you can run:

```bash
protractor conf.js
```

This might run the app in a new browser tab if declared in the `spec.js` file.<br />

## Learn More

You can learn [Protractor](https://www.protractortest.org/#/) setup, check out the [Tutotial](https://www.protractortest.org/#/tutorial).

### Simple Setup For Protractor With Angular 8 Project

**Add** [webdriver-manager](https://www.npmjs.com/package/webdriver-manager) **and** [protractor](https://www.npmjs.com/package/protractor) **globally, so that we can run our codes from terminal easily.**

```bash
sudo npm install -g webdriver-manager protractor
```

**Start the Selenium Server.**
```bash
sudo webdriver-manager start
```

**Manually create a directory for your project. And add 2 files `conf.js` and `spec.js` in the project directory.**

**Let's update the** `spec.js` **file with some Protractor codes.**

Add the following codes:
```js
describe('Protractor find greatest', () => {
  var n0 = element(by.css('.n0'));
  var n1 = element(by.css('.n1'));
  var n2 = element(by.css('.n2'));

  var findBtn = element(by.id('find-btn'));

  var resultText = element(by.id('result-text'));

  beforeEach(function() {
    browser.get('https://musing-khorana-85376c.netlify.com/');
  });

  it('should find the greatest number', function() {
    n0.sendKeys(-11);
    n1.sendKeys(1);
    n2.sendKeys(2);

    findBtn.click();

    expect(resultText.getText()).toEqual('2');
  });
});
```

The above Protractor testing code opens a browser window & visits the `https://musing-khorana-85376c.netlify.com/` URL.

Then it finds the elements with class names `n0`, `n1` and `n2` and also finds the elements with ids `find-btn` and `result-text`.

If all the elements are available in the browser DOM it passes the values `-11`, `1` and `2` to `n0`, `n1` and `n2` elements respectively. And triggers a click event on the `find-btn` element.

Finally, it waits for the `result-text` element to be updated with the result value.

It matches the updated text is `2` or not. And shows some result in the terminal.

**Update the** `conf.js` **file with required specs.**

Add the following codes:
```js
exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['spec.js']
}
```

**Hurray!! we are ready to go now.**

Runs the testing codes from your Protractor project root.

Launch the terminal & goto the project root. And add the below codes:

```bash
protractor conf.js
```
