
/**
 * @description Test an addition operation.
 */
/* describe('Protractor Demo App', function() {
  it('should add one and two', function() {
    browser.get('http://juliemr.github.io/protractor-demo/');
    element(by.model('first')).sendKeys(1);
    element(by.model('second')).sendKeys(2);

    element(by.id('gobutton')).click();

    expect(element(by.binding('latest')).getText()).
        toEqual('3'); // This is correct!
        // toEqual('4'); // This is wrong!
  });
}); */

/**
 * @description Test multiple addition operations.
 */
/* describe('Protractor Demo App', function() {
  var firstNumber = element(by.model('first'));
  var secondNumber = element(by.model('second'));
  var goButton = element(by.id('gobutton'));
  var latestResult = element(by.binding('latest'));

  beforeEach(function() {
    browser.get('http://juliemr.github.io/protractor-demo/');
  });

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('Super Calculator');
  });

  it('should add one and two', function() {
    firstNumber.sendKeys(1);
    secondNumber.sendKeys(2);

    goButton.click();

    expect(latestResult.getText()).toEqual('3');
  });

  it('should read the value from an input', function() {
    firstNumber.sendKeys(1);

    // The getAttribute() always returns some value.
    expect(firstNumber.getAttribute('value')).toEqual('1');
  });

  it('can not read the idea from the input', function() {
    firstNumber.sendKeys(1);

    // The getAttribute() always returns some value.
    expect(firstNumber.getAttribute('idea')).toEqual(null);
  });

  it('should add four and six', function() {
    // Fill this in.
    expect(latestResult.getText()).toEqual('10');
  });
}); */

/**
 * @description Find the greatest in given numbers in Angular 8 framework.
 */
/* describe('Protractor testing application', () => {
  var n0 = element(by.css('.n0'));
  var n1 = element(by.css('.n1'));
  var n2 = element(by.css('.n2'));

  var findBtn = element(by.id('find-btn'));

  var resultText = element(by.id('result-text'));

  beforeEach(function() {
    browser.get('http://localhost:4004/');
  });

  it('should find the greatest number', function() {
    n0.sendKeys(-11);
    n1.sendKeys(1);
    n2.sendKeys(2);

    findBtn.click();

    expect(resultText.getText()).toEqual('2');
  });
}); */

/**
 * @description Sort the given numbers in Angular 8 framework.
 */
describe('Protractor async testing application', () => {
  beforeEach(function() {
    browser.get('https://musing-khorana-85376c.netlify.com/async');
  });

  var text = element(by.css('.n0'));

  var findBtn = element(by.id('find-btn'));

  var resultText = element(by.id('result-text'));

  it('should match with the response text', function() {
    text.clear();
    text.sendKeys('9, 12,-3, 48, -6');

    findBtn.click();

    expect(resultText.getText()).toEqual('-6, -3, 9, 12, 48');
  });
});